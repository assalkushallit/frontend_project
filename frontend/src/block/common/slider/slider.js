$(function () {
	$('.b-slider__container').slick({
		slidesToShow: 1,
		asNavFor: '.b-slider__nav',
	});

	$('.b-slider__nav').slick({
		slidesToShow: 5,
		asNavFor: '.b-slider__container',
		focusOnSelect: true,
	})
});