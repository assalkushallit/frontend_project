$(function () {
	$('.uk-navbar').not('.uk-navbar-nav').on('click', function () {
		$(this).toggleClass('uk-navbar-opened');
	});
});