$(function () {
	$('.b-main-slider').each(function () {
		if ($(this).hasClass('b-main-slider--profiles')) {
			$(this).slick({
				slidesToShow: 5,
				dots: true,
				responsive: [
					{
						breakpoint: 1024,
						settings: {
							slidesToShow: 3,
						}

					},
					{
						breakpoint: 720,
						settings: {
							slidesToShow: 1,
							arrows: false,
						}
					},
				],
			});
		} else {
			$(this).slick({
				slidesToShow: 4,
				dots: true,
				responsive: [
					{
						breakpoint: 1024,
						settings: {
							slidesToShow: 2,
						}
					},
					{
						breakpoint: 720,
						settings: {
							slidesToShow: 1,
							arrows: false,
						}
					},
				],
			});
		}
	});
});