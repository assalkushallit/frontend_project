$(function () {
	$('.b-header__enter').magnificPopup({
		items: {
			src: '#enterForm',
			type: 'inline',
		}
	});

	$('.b-header__password-restore').magnificPopup({
		items: {
			src: '#passwordRestore',
			type: 'inline',
		}
	});

	$('.b-header__register').magnificPopup({
		items: {
			src: '#registrationForm',
			type: 'inline',
		}
	});
});