import 'jquery-validation';

$(function () {
	$('.b-form__field').on('focusin', function() {
		$(this).addClass('b-form__field--focused');
	}).on('focusout change', function() {
		if ($(this).children('input').length) {
			if ($(this).children('input').val() === '')  $(this).removeClass('b-form__field--focused');
		} else if ($(this).find('select option:selected').length) {
			if ($(this).find('select option:selected').text() === '') $(this).removeClass('b-form__field--focused');
		} else if ($(this).find('textarea').length) {
			if ($(this).find('textarea').val() === '') $(this).removeClass('b-form__field--focused');
		}
	});

	// Правила для валидации
	var rules = {};

	// Сообщения для валидации
	var messages = {};

	$('.b-form__input').each(function () {
		rules[$(this).attr('name')] = {
			required: true,
		};

		messages[$(this).attr('name')] = {
			required: 'Заполните ' + ($(this).prev('label').length > 0 ? '' : 'это ') + 'поле' + ($(this).prev('label').length > 0 ? ' ' + $(this).prev('label').html() : '.'),
		};
	});

	$('.b-form').validate({
		debug: true,
		rules: rules,
		messages: messages,
	});
});