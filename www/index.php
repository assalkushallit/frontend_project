<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>
<body>
<?php include 'header.php'; ?>

<main class="main-container uk-container uk-flex">

    <div class="content">

<!--        хлебные крошки, заголовок, переключатель режима отображения страницы (список\карта)-->
        <div class="uk-container-custom-padding">
			<?php include 'breadcrumbs.php'; ?>
            <div class="uk-flex-middle" uk-grid>
                <h1 class="uk-heading-primary">КАТАЛОГ</h1>
                <div>
                    <ul class="uk-subnav uk-subnav-switch"
                        uk-switcher="animation: uk-animation-fade; connect: .switcher-container">
                        <li class="uk-active">Списком</li>
                        <li>На карте</li>
                    </ul>
                </div>
            </div>
        </div>

<!--        фильтр-->
        <div class="uk-container-custom-padding">
            <div class="uk-container c-bordered">
                <?php include 'filter.php'; ?>
            </div>
        </div>

<!--        контейнер с режимами страницы (список\карта)-->
        <div class="uk-container-custom-padding">
            <ul class="uk-switcher switcher-container">
                <li uk-switcher-item="0">

<!--                    одна запись-->
                    <div class="uk-flex-nowrap" uk-grid>
                        <img src="img/pic.png" srcset="img/pic@2x.png 2x, img/pic@3x.png 3x" class="pic">
                        <div class="c-border-bottom-muted">
                            <div class="uk-grid-divider uk-flex-nowrap" uk-grid>
                                <div class="c-filter-location">
                                    <a class="uk-link-heading" href="/">Необычная 3-х комнатная квартира на юге
                                        Калининграда</a>
                                </div>
                                <div>
                                    <div class="uk-flex uk-flex-middle uk-height-1-1 uk-flex-nowrap" uk-grid>
                                        <div class="c-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26"
                                                 viewBox="0 0 26 26">
                                                <g fill="none" fill-rule="evenodd" stroke-width="2"
                                                   transform="translate(1 1)">
                                                    <circle cx="12" cy="12" r="12" stroke="#D0D0D0"/>
                                                    <path stroke="#4A90E2" stroke-linecap="round"
                                                          d="M12.02 6v12.042M6 12h12.042"/>
                                                </g>
                                            </svg>
                                            <a href="/">
                                                Добавить<br>заметку
                                            </a>
                                        </div>
                                        <div class="c-icon">
                                            <div class="uk-badge">3</div>
                                            &nbsp;<a href="/">Сообщения</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div uk-grid>
                                <div class="c-two-sided-list">
                                    <div>7 500 000 ₽</div>
                                    <div>1 235 ₽ за м²</div>
                                    <div>Комиссия 3%</div>
                                </div>
                                <div class="c-two-sided-list">
                                    <div>120 м²</div>
                                    <div>Кухня 17м²</div>
                                    <div>Жилая 98м²</div>
                                </div>
                                <div class="c-two-sided-list">
                                    <div>3 комнаты</div>
                                    <div>Новостройка</div>
                                    <div>свободно с<br>12.05.17</div>
                                </div>
                                <div class="c-two-sided-list">
                                    <div>8 этаж из 23</div>
                                    <div>Гараж/парковка</div>
                                </div>
                            </div>
                            <div class="uk-flex">
                                <div class="uk-padding-small uk-padding-remove-top uk-padding-remove-left">
                                    Продам 3х комнатную квартиру (евродвушка), в монолитно-кирпичном доме Бизнес-класса
                                    ЖК"Невский". Квартира светлая (юго-западная сторона), тёплая (деревянные окна),
                                    высота
                                    потолка 3,10 м, санузел раздел
                                </div>
                                <div class="c-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <g fill="none" fill-rule="evenodd" stroke="#4A90E2" stroke-linecap="round"
                                           stroke-linejoin="round" stroke-width="2">
                                            <path d="M3 11.068h6.943v10.58H3zM9.943 11.068h10.415v10.58H9.943zM3 2h10.415v9.068H3z"/>
                                        </g>
                                    </svg>
                                    <a href="/">Схема</a>
                                </div>
                            </div>
                            <div class="uk-text-small uk-text-muted c-under-row-text">ID: 98085475 Объект: 12545854
                                Опубликовано: 25.08.2015 в 11:30 Просмотров: 132 (помечено 14)
                            </div>
                        </div>
                        <div class="c-border-bottom-muted">
                            <div class="uk-container-custom-padding uk-background-muted uk-height-1-1">
                                <div class="c-icon uk-padding-small uk-padding-remove-top uk-padding-remove-left">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path fill="#4A90E2" fill-rule="evenodd"
                                              d="M21.444 2.5a.47.47 0 0 0-.172.026l-5.939 2.19L8.667 2.5 2.406 4.506c-.234.073-.406.263-.406.506v15.96c0 .29.25.528.556.528a.47.47 0 0 0 .172-.026l5.939-2.19 6.666 2.216 6.267-2c.233-.08.4-.27.4-.512V3.028c0-.29-.25-.528-.556-.528zm-6.11 16.889l-6.667-2.222V4.61l6.666 2.222V19.39z"/>
                                    </svg>
                                    <a href="/">На карте</a>
                                </div>
                                <div class="c-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path fill="none" fill-rule="evenodd" stroke="#C74051" stroke-width="2"
                                              d="M16.117 4.323c-.113 0-.226.004-.338.013-1.698.127-2.71 1.1-3.779 2.08-.981-.915-2.081-1.953-3.779-2.08a4.55 4.55 0 0 0-.338-.013c-1.353 0-2.736.6-3.554 1.795-1.44 2.102-.439 5.114.917 7.265C7.49 16.94 12 19.677 12 19.677s4.511-2.736 6.754-6.294c1.356-2.151 2.356-5.163.917-7.265-.817-1.195-2.201-1.795-3.554-1.795"/>
                                    </svg>
                                    <a href="/">Пометить</a>
                                </div>
                                <hr>
                                <div class="uk-text-small uk-text-muted">Продавец</div>
                                <a href="/">Selix Realty</a>
                                <div class="c-rating">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path fill="#FFB96D" fill-rule="evenodd"
                                              d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path fill="#FFB96D" fill-rule="evenodd"
                                              d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <g fill="none" fill-rule="evenodd">
                                            <path fill="#D8D8D8"
                                                  d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                            <path fill="#FFB96D"
                                                  d="M12 1v18l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1z"/>
                                        </g>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path fill="#D8D8D8" fill-rule="evenodd"
                                              d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path fill="#D8D8D8" fill-rule="evenodd"
                                              d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--        постраничка-->
                    <div class="pagenav">
                        <a href="/" class="link active">1</a>
                        <a href="/" class="link">2</a>
                        <a href="/" class="link">3</a>
                        <a href="/" class="link">4</a>
                        <div class="divider">...</div>
                        <a href="/" class="link">14</a>
                        <a href="/" class="button">Следующая страница</a>
                    </div>
                </li>
                <li uk-switcher-item="1">2</li>
            </ul>
        </div>

    </div>
</main>
<?php include 'footer.php'; ?>
</body>
</html>
