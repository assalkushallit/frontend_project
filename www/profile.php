<?php include 'head.php'; ?>
<?php include 'header.php'; ?>

<main class="main-container uk-container uk-flex uk-flex-top">

    <!--    боковая панель-->
    <div class="sidebar c-bordered">
        <div class="c-heading-2">ПРОФИЛЬ</div>
        <div class="uk-button uk-button-small uk-background-muted">Недвижимость</div>
        <hr>
        <div class="uk-flex uk-flex-between">
            <div class="c-heading-3">Календарь</div>
            <span class="count">15 (5 сегодня)</span>
        </div>
        <hr>
        <div class="c-heading-3">Объекты</div>
        <div class="uk-flex uk-flex-between">
            <a href="/">Активные</a>
            <span class="count">2</span>
        </div>
        <div class="uk-flex uk-flex-between">
            <div class="c-heading-3">Входящие предложения</div>
            <div class="uk-badge">10</div>
        </div>
        <div class="uk-flex uk-flex-between">
            <div class="c-heading-3">Сообщения по объектам</div>
            <div class="uk-badge-reverse">2</div>
        </div>
        158 872 ₽
        <div class="uk-text-danger">заполнено 16%</div>
    </div>

    <div class="content">
        <!--        хлебные крошки, заголовок, кнопки справа от заголовка-->
        <div class="uk-container-custom-padding">
            <?php include 'breadcrumbs.php' ?>
            <h1 class="uk-heading-primary uk-text-uppercase">Настройки профиля</h1>
            <div class="uk-container-custom-padding c-bordered">
                <div class="uk-grid">
                <div class="b-form">
                    <div>
                        <div class="b-form__field b-form__field--type-rounded">
                            <input type="text" value="" class="b-form__input">
                            <label class="b-form__label">Фамилия</label>
                        </div>
                    </div>
                    <div>
                        <div class="b-form__field b-form__field--type-rounded">
                            <input type="text" value="" class="b-form__input">
                            <label class="b-form__label">Имя</label>
                        </div>
                    </div>
                    <div>
                        <div class="b-form__field b-form__field--type-rounded">
                            <input type="text" value="" class="b-form__input">
                            <label class="b-form__label">Отчество</label>
                        </div>
                    </div>
                </div>
                <div class="uk-text-center">
                    <div>
                        <img src="img/avatar.jpg" srcset="img/avatar@2x.jpg 2x" class="avatar" width="200" height="184">
                    </div>
                    <button class="uk-button uk-button-primary-inverse">Изменить фото</button>
                </div>
            </div>
                <div class="uk-margin-small-top">
                    <div class="uk-h4">Дата рождения:</div>
                    <div class="b-form">
                        <div uk-grid>
                            <div class="uk-width-1-5">
                                <div class="b-form__field b-form__field--type-rounded">
                                    <label class="b-form__label">Число</label>
                                    <select class="b-select b-form__dropdown b-form__dropdown--gray b-form__dropdown--type-rounded" name="test-1ee" id="test-1ee">
                                        <option value="NULL"></option>
                                        <?php
                                        for ($i=0; $i<32; $i++) { ?>
                                            <option value="<?= $i ?>"><?= $i ?></option>
                                        <?php };
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-1-5">
                                <div class="b-form__field b-form__field--type-rounded">
                                    <label class="b-form__label">Месяц</label>
                                    <select class="b-select b-form__dropdown b-form__dropdown--type-rounded b-form__dropdown--gray" name="test-233" id="test-233">
                                        <option value="NULL"></option>
                                        <option value="1">Января</option>
                                        <option value="2">Февраля</option>
                                        <option value="3">Марта</option>
                                        <option value="4">Апреля</option>
                                        <option value="5">Мая</option>
                                        <option value="6">Июня</option>
                                        <option value="7">Июля</option>
                                        <option value="8">Августа</option>
                                        <option value="9">Сентября</option>
                                        <option value="10">Октября</option>
                                        <option value="11">Ноября</option>
                                        <option value="12">Декабря</option>
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-1-5">
                                <div class="b-form__field b-form__field--type-rounded">
                                    <label class="b-form__label">Год</label>
                                    <input type="text" value="" class="b-form__input b-form__input--gray">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="uk-margin">

                <div>
                    <div class="uk-h4">Текущий тип пользователя: <span class="uk-text-primary">Юридическое лицо</span></div>
                    <div class="uk-text-small">
                        Вы можете сменить тип пользователя, получив таким образом доступ к расширенным настройкам профиля.
                    </div>
                    <div uk-grid class="uk-margin-top">
                        <div class="uk-width-1-3">
                            <div class="b-form__field b-form__field--radio">
                                <input type="radio" name="user_type" id="individual">
                                <label for="individual">Физическое лицо</label>
                            </div>
                        </div>
                        <div class="uk-width-1-3">
                            <div class="b-form__field b-form__field--radio">
                                <input type="radio" name="user_type" id="entrepreneur">
                                <label for="entrepreneur">Индивидуальный предпр</label>
                            </div>
                        </div>
                        <div class="uk-width-1-3">
                            <div class="b-form__field b-form__field--radio">
                                <input type="radio" name="user_type" id="entity" checked>
                                <label for="entity">Юридическое лицо</label>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="uk-margin">

                <div class="block">
                    <div class="uk-h4">Настройка юридического лица</div>
                    <div class="uk-margin-bottom">
                        <button class="uk-button uk-button-primary-inverse">Добавить юридическое лицо</button>
                        <button class="uk-button uk-button-primary-inverse">Существующее юридическое лицо</button>
                    </div>
                    <div uk-grid class="uk-margin-bottom">
                        <div class="uk-width-1-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <label class="b-form__label">Должность</label>
                                <input type="text" value="" class="b-form__input b-form__input--non-active">
                            </div>
                        </div>
                    </div>
                    <div uk-grid>
                        <div class="uk-width-1-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <label class="b-form__label">Обращение</label>
                                <select class="b-select b-form__dropdown b-form__dropdown--type-rounded" name="test-2322" id="test-2322">
                                    <option value="NULL"></option>
                                    <option value="1">Господин</option>
                                    <option value="2">Госпожа</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-1-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <label class="b-form__label">Контактный телефон</label>
                                <input type="text" value="" class="b-form__input b-form__input--non-active">
                            </div>
                        </div>
                        <div class="uk-width-1-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <label class="b-form__label">Контактный email</label>
                                <input type="text" value="" class="b-form__input b-form__input--non-active">
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="uk-margin">

                <div>
                    <div class="uk-h4">Ваша специализация</div>
                    <div uk-grid>
                        <div class="uk-width-1-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <label class="b-form__label">Покупатели</label>
                                <select class="b-select b-form__dropdown b-form__dropdown--type-rounded" name="test-2" id="test-2">
                                    <option value="NULL"></option>
                                    <option value="1">111</option>
                                    <option value="2">222</option>
                                </select>
                            </div>
                        </div>
                </div>

                <hr class="uk-margin">

                <div class="block">
                    <div class="uk-h4">Смена пароля</div>
                    <div uk-grid>
                        <div class="uk-width-1-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <label class="b-form__label">Старый пароль</label>
                                <input type="password" value="" class="b-form__input b-form__input--non-active">
                            </div>
                        </div>
                    </div>
                    <div uk-grid>
                        <div class="uk-width-1-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <label class="b-form__label">Новый пароль</label>
                                <input type="text" value="" class="b-form__input b-form__input--non-active">
                            </div>
                        </div>
                    </div>
                    <div uk-grid>
                        <div class="uk-width-1-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <label class="b-form__label">Повторите новый пароль</label>
                                <input type="text" value="" class="b-form__input b-form__input--non-active">
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="uk-margin">

                <div class="uk-text-center">
                    <button class="uk-button uk-button-primary uk-width-1-3">Сохранить</button>
                </div>
            </div>
        </div>
</main>
<?php include 'footer.php'; ?>

