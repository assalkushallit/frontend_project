<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>
<body>
<?php include 'header.php'; ?>

<main class="main-container uk-container uk-flex">

    <div class="content">

        <!--        хлебные крошки, заголовок, переключатель режима отображения страницы (список\карта)-->
        <div class="uk-container-custom-padding">
			<?php include 'breadcrumbs.php'; ?>
            <div class="uk-flex-middle" uk-grid>
                <h1 class="uk-heading-primary">ФОРМЫ</h1>
            </div>
        </div>

        <div class="b-form">
            <div class="b-form__layout">
                <div class="b-form__field b-form__field--type-rounded">
                    <label class="b-form__label">label</label>
                    <input type="text" value="" class="b-form__input">
                </div>
                <div class="b-form__field b-form__field--focused b-form__field--type-rounded">
                    <label class="b-form__label">label</label>
                    <input type="text" value="Filled text" class="b-form__input b-form__input--blue">
                </div>
                <div class="b-form__field b-form__field--type-rounded">
                    <label class="b-form__label">label</label>
                    <input type="text" value="" class="b-form__input b-form__input--error">
                </div>
                <div class="b-form__field b-form__field--type-rounded">
                    <label class="b-form__label">label</label>
                    <input type="text" value="" class="b-form__input b-form__input--non-active">
                </div>
                <div class="b-form__field b-form__field--type-rounded b-form__field--mail">
                    <label class="b-form__label">label</label>
                    <input type="text" value=""
                           class="b-form__input b-form__input--blue">
                </div>
                <div class="b-form__field b-form__field--type-underline">
                    <label class="b-form__label">label</label>
                    <input type="text" class="b-form__input">
                </div>
                <div class="b-form__field b-form__field--type-underline">
                    <label class="b-form__label">label</label>
                    <input type="text" class="b-form__input b-form__input--blue">
                </div>
                <div class="b-form__field b-form__field--type-underline">
                    <label class="b-form__label">label</label>
                    <input type="text" class="b-form__input b-form__input--error">
                </div>
                <div class="b-form__field b-form__field--type-underline">
                    <label class="b-form__label">label</label>
                    <input type="text" class="b-form__input b-form__input--non-active">
                </div>
                <div class="b-form__field b-form__field--type-underline b-form__field--mail">
                    <label class="b-form__label">label</label>
                    <input type="text" class="b-form__input b-form__input--blue">
                </div>
                <div class="b-form__field b-form__field--type-rounded">
                    <label class="b-form__label">label</label>
                    <select class="b-select b-form__dropdown b-form__dropdown--type-rounded" name="test-1" id="test-1">
                        <option value="NULL"></option>
                        <option value="1">Dropdown option 1</option>
                        <option value="2">Dropdown option 2</option>
                        <option value="3">Dropdown option 3</option>
                        <option value="4">Dropdown option 4</option>
                    </select>
                </div>
                <div class="b-form__field b-form__field--focused b-form__field--type-underline">
                    <label class="b-form__label">label</label>
                    <select class="b-select b-form__dropdown b-form__dropdown--type-underline" name="test-2" id="test-2">
                        <option value="NULL"></option>
                        <option value="1" selected>Dropdown option 1</option>
                        <option value="2">Dropdown option 2</option>
                        <option value="3">Dropdown option 3</option>
                        <option value="4">Dropdown option 4</option>
                    </select>
                </div>
            </div>
        </div>

    </div>
</main>
<?php include 'footer.php'; ?>
</body>
</html>