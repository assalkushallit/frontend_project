<div class="uk-container b-header">
    <div class="uk-container-custom-padding uk-background-muted">
        <div class="uk-flex-middle uk-flex-nowrap" uk-grid>
            <a href="/" class="b-header__logo"><img src="img/logo.svg" class="logo"></a>
            <div class="uk-text-muted c-pointer c-primary-on-hover b-header__search" uk-toggle="target: .search-input">
                <button uk-icon="icon: search-navbar; ratio: 0.9" class="uk-text-middle"></button>&nbsp;&nbsp;<span
                        class="uk-text-middle">Поиск</span>
            </div>
            <div class="search-input" hidden><input type="text" name="q"></div>
            <a href="/catalog/" class="uk-link-text uk-navbar-right b-header__link">Каталог</a>
            <div><a class="uk-button uk-button-primary-inverse b-header__contact">Подать объявление</a></div>
            <div><a class="uk-button uk-box-shadow-large uk-button-primary b-header__enter">Войти</a></div>
        </div>
    </div>

    <div id="enterForm" class="mfp-hide b-header__autorization-form">
        <div class="uk-text-center uk-heading-primary">АВТОРИЗАЦИЯ</div>
        <br><br>
        <div class="uk-flex-around" uk-grid>
            <form class="b-form uk-width-expand" action="/">
                <div class="b-form__layout">
                    <h4 class="uk-text-uppercase uk-text-center">По логину и паролю:</h4>
                </div>
                <div class="b-form__layout">
                    <div class="b-form__field b-form__field--type-rounded">
                        <label class="b-form__label">Логин</label>
                        <input type="text" name="login" value="" class="b-form__input b-form__input--non-active">
                    </div>
                </div>
                <div class="b-form__layout">
                    <div class="b-form__field b-form__field--type-rounded">
                        <label class="b-form__label">Пароль</label>
                        <input type="text" name="password" value="" class="b-form__input b-form__input--non-active">
                    </div>
                </div>
                <div class="b-form__layout">
                    <div class="uk-flex uk-flex-between b-form__field">
                        <a href="/" class="b-header__register">Регистрация</a>
                        <a href="/" class="b-header__password-restore">Забыли пароль?</a>
                    </div>
                </div>
                <div class="b-form__layout">
                    <div class="b-form__field">
                        <button type="submit" class="uk-button uk-button-large uk-box-shadow-large uk-button-primary">
                            Войти
                        </button>
                    </div>
                </div>
            </form>
            <div class="uk-width-expand uk-text-center">
                <h4 class="uk-text-uppercase uk-text-center">Через социальные сети:</h4>
                <a href="/" class="uk-icon-button uk-icon-button--facebook uk-icon-button-bigger"><i class="fa fa-facebook-f"></i></a>
                <a href="/" class="uk-icon-button uk-icon-button--twitter uk-icon-button-bigger"><i class="fa fa-twitter"></i></a>
                <a href="/" class="uk-icon-button uk-icon-button--vk uk-icon-button-bigger"><i class="fa fa-vk"></i></a>
                <a href="/" class="uk-icon-button uk-icon-button--google-plus uk-icon-button-bigger"><i class="fa fa-google-plus"></i></a>
            </div>
        </div>
    </div>

    <div id="passwordRestore" class="mfp-hide b-header__password-restore-form">
        <div class="uk-text-center uk-heading-primary">ВОССТАНОВЛЕНИЕ<br>ПАРОЛЯ</div>
        <br><br>
        <div class="uk-flex-around" uk-grid>
            <form class="b-form uk-width-expand" action="/">
                <div class="b-form__layout">
                    <div class="b-form__field uk-text-center">
                        <span class="uk-text-small">Укажите почтовый адрес, который Вы использовали при регистрации аккаунта.</span>
                    </div>
                </div>
                <div class="b-form__layout">
                    <div class="b-form__field b-form__field--type-rounded">
                        <label class="b-form__label">Логин</label>
                        <input type="text" value="" class="b-form__input b-form__input--non-active">
                    </div>
                </div>
                <div class="b-form__layout">
                    <div class="b-form__field uk-text-center">
                        <span class="uk-text-small">Контрольная строка для смены пароля, а также Ваши регистрационные данные будут высланы Вам по e-mail.</span>
                    </div>
                </div>
                <div class="b-form__layout">
                    <div class="b-form__field">
                        <button type="submit" class="uk-button uk-button-large uk-box-shadow-large uk-button-primary">
                            Выслать
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="registrationForm" class="mfp-hide b-header__register-form">
        <div class="uk-text-center uk-heading-primary">РЕГИСТРАЦИЯ</div>
        <br><br>
        <div class="uk-flex-around" uk-grid>
            <form class="b-form uk-width-expand" action="/">
                <div class="b-form__layout">
                    <div class="b-form__field b-form__field--type-rounded">
                        <label class="b-form__label">E-mail*</label>
                        <input type="text" value="" class="b-form__input b-form__input--non-active">
                    </div>
                </div>
                <div class="b-form__layout">
                    <div class="b-form__field b-form__field--type-rounded">
                        <label class="b-form__label">Имя</label>
                        <input type="text" value="" class="b-form__input b-form__input--non-active">
                    </div>
                </div>
                <div class="b-form__layout">
                    <div class="b-form__field b-form__field--type-rounded">
                        <label class="b-form__label">Телефон</label>
                        <input type="text" value="" class="b-form__input b-form__input--non-active">
                    </div>
                </div>
                <div class="b-form__layout">
                    <div class="b-form__field b-form__field--type-underline">
                        <label class="b-form__label">Тип профиля*</label>
                        <select class="b-select b-form__dropdown b-form__dropdown--type-underline" name="test-2"
                                id="test-2">
                            <option value="NULL"></option>
                            <option value="1">Юридическое лицо</option>
                            <option value="2">Физическое лицо</option>
                        </select>
                    </div>
                </div>
                <div class="b-form__layout">
                    <div class="b-form__field">
                        <span class="c-footnote">¹ При регистрации как юридическое лицо, у пользователя есть возможность прикрепить аккаунт к существующему юридическому лицу или создать новое (если оно отсутствует в каталоге). При регистрации как физическое лицо, у пользователя нет возможности создания юридического лица или прикрепляться к существующему. Тип профиля в дальнейшем можно изменить в настройках аккаунта.</span>
                    </div>
                </div>
                <div class="b-form__layout">
                    <div class="b-form__field b-form__field--type-rounded">
                        <label class="b-form__label">Пароль</label>
                        <input type="text" value="" class="b-form__input b-form__input--non-active">
                    </div>
                </div>
                <div class="b-form__layout">
                    <div class="b-form__field b-form__field--type-rounded">
                        <label class="b-form__label">Повторите пароль</label>
                        <input type="text" value="" class="b-form__input b-form__input--non-active">
                    </div>
                </div>
                <div class="b-form__layout">
                    <div class="b-form__field">
                        <button type="submit" class="uk-button uk-button-large uk-box-shadow-large uk-button-primary">
                            Зарегистрироваться
                        </button>
                    </div>
                </div>
            </form>
            <div class="uk-width-expand uk-text-center">
                <h4 class="uk-text-uppercase uk-text-center">Через социальные сети:</h4>
                <a href="/" class="uk-icon-button uk-icon-button--facebook uk-icon-button-bigger"><i class="fa fa-facebook-f"></i></a>
                <a href="/" class="uk-icon-button uk-icon-button--twitter uk-icon-button-bigger"><i class="fa fa-twitter"></i></a>
                <a href="/" class="uk-icon-button uk-icon-button--vk uk-icon-button-bigger"><i class="fa fa-vk"></i></a>
                <a href="/" class="uk-icon-button uk-icon-button--google-plus uk-icon-button-bigger"><i class="fa fa-google-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="uk-container">
    <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
        <ul class="uk-navbar-nav uk-flex-wrap uk-flex-around uk-width-1-1">
            <li><a href="" class="uk-text-large">Недвижимость</a></li>
            <li><a href="" class="uk-text-large">Оценка</a></li>
            <li><a href="" class="uk-text-large">Кредитование</a></li>
            <li><a href="" class="uk-text-large">Переезд</a></li>
            <li><a href="" class="uk-text-large">Уборка</a></li>
        </ul>
        <hr>
    </nav>
</div>