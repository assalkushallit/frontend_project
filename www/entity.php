<?php include 'head.php'; ?>
<?php include 'header.php'; ?>

<main class="main-container uk-container uk-flex uk-flex-top">

    <!--    боковая панель-->
    <div class="sidebar c-bordered">
        <div class="c-heading-2">ПРОФИЛЬ</div>
        <div class="uk-button uk-button-small uk-background-muted">Недвижимость</div>
        <hr>
        <div class="uk-flex uk-flex-between">
            <div class="c-heading-3">Календарь</div>
            <span class="count">15 (5 сегодня)</span>
        </div>
        <hr>
        <div class="c-heading-3">Объекты</div>
        <div class="uk-flex uk-flex-between">
            <a href="/">Активные</a>
            <span class="count">2</span>
        </div>
        <div class="uk-flex uk-flex-between">
            <div class="c-heading-3">Входящие предложения</div>
            <div class="uk-badge">10</div>
        </div>
        <div class="uk-flex uk-flex-between">
            <div class="c-heading-3">Сообщения по объектам</div>
            <div class="uk-badge-reverse">2</div>
        </div>
        158 872 ₽
        <div class="uk-text-danger">заполнено 16%</div>
    </div>

    <div class="content">
        <!--        хлебные крошки, заголовок, кнопки справа от заголовка-->
        <div class="uk-container-custom-padding">
            <?php include 'breadcrumbs.php' ?>
            <h1 class="uk-heading-primary uk-text-uppercase">Новое юридическое лицо</h1>
            <div class="uk-container-custom-padding c-bordered">
                <div uk-grid>
                    <div class="b-form uk-width-2-3">
                        <div>
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">Название юридического лица</label>
                            </div>
                        </div>
                        <div class="uk-width-1-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <label class="b-form__label">Вид деятельности</label>
                                <select class="b-select b-form__dropdown b-form__dropdown--gray b-form__dropdown--type-rounded" name="test-videee" id="test-videee">
                                    <option value="NULL"></option>
                                    <option value="val1">val1</option>
                                    <option value="val2">val2</option>
                                </select>
                            </div>
                        </div>
                        <div>
                            <div class="b-form__field b-form__field--type-rounded">
                                <textarea class="b-form__textarea b-form__textarea--gray"></textarea>
                                <label class="b-form__label">Описание</label>
                            </div>
                        </div>
                    </div>
                    <div class="uk-text-center uk-width-1-3">
                        <div>
                            <img src="img/avatar.jpg" srcset="img/avatar@2x.jpg 2x" class="avatar" width="200" height="184">
                        </div>
                        <button class="uk-button uk-button-primary-inverse">Добавить логотип</button>
                    </div>
                </div>
                <hr class="uk-margin">
                <div class="uk-margin-small-top">
                    <div class="uk-h4">Контактная информация</div>
                    <div class="b-form">
                        <div uk-grid>
                            <div class="uk-width-1-3">
                                <div class="b-form__field b-form__field--type-rounded">
                                    <input type="text" value="" class="b-form__input b-form__input--gray">
                                    <label class="b-form__label">Контактный e-mail</label>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="b-form__field b-form__field--type-rounded">
                                    <input type="text" value="" class="b-form__input b-form__input--gray">
                                    <label class="b-form__label">Контактный телефон</label>
                                </div>
                            </div>
                            <div class="uk-width-1-3">
                                <div class="b-form__field b-form__field--type-rounded">
                                    <input type="text" value="" class="b-form__input b-form__input--gray">
                                    <label class="b-form__label">Адрес сайта</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="uk-margin">

                <div>
                    <div class="uk-h4">Реквизиты</div>
                    <div uk-grid class="uk-margin">
                        <div class="uk-width-1-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <label class="b-form__label">Тип организации</label>
                                <select class="b-select b-form__dropdown b-form__dropdown--gray b-form__dropdown--type-rounded" name="test-vid" id="test-vid">
                                    <option value="NULL"></option>
                                    <option value="val1">val1</option>
                                    <option value="val2">val2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div uk-grid>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">ИНН</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">Сокращенное наименование</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">Полное название организации</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">ОГРН</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">КПП</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">Дата государственной регистрации</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">ОКПО</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">ОКТМО</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">Ген. директор</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">Гл. бухгалтер</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">Юридический адрес</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">Фактический адрес</label>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="uk-margin">

                <div class="block">
                    <div class="uk-h4">Реквизиты</div>
                    <div uk-grid>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">Наименование банка</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">БИК</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">Расчетный счет</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">Кор. счет</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">Валюта счета</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">Адрес банка</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <input type="text" value="" class="b-form__input b-form__input--gray">
                                <label class="b-form__label">SWIFT</label>
                            </div>
                        </div>
                        <div class="uk-width-2-3">
                            <div class="b-form__field b-form__field--type-rounded">
                                <textarea class="b-form__textarea b-form__textarea--gray"></textarea>
                                <label class="b-form__label">Комментарий</label>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="uk-margin">

                <div class="uk-text-center">
                    <button class="uk-button uk-button-default">Отменить</button>
                    <button class="uk-button uk-button-primary">Сохранить и отправить на проверку</button>
                </div>

            </div>
            <div class="uk-container-custom-padding c-bordered uk-margin">
                <h2 class="uk-text-uppercase">Поиск юридического лица</h2>
                <hr class="uk-margin">
                <div uk-grid>
                    <div class="uk-width-4-5">
                        <div class="b-form__field b-form__field--type-rounded">
                            <input type="text" value="" class="b-form__input b-form__input--blue">
                        </div>
                    </div>
                    <div uk-grid class="width-1-5">
                        <button class="uk-button uk-button-large uk-button-primary uk-width">Найти</button>
                    </div>
                </div>
                <div class="uk-margin-top">
                    Результаты поиска
                </div>
                <hr class="uk-margin">
                <div class="entity__row uk-childr" uk-grid>
                    <div class="entity__image uk-width-auto">
                        <img src="img/avatar.jpg" srcset="img/avatar@2x.jpg 2x" class="avatar" width="80" height="80">
                    </div>
                    <div class="uk-width-expand">
                        <div><a href="#">Universal home</a></div>
                        <div class="uk-flex">
                            <div>*****</div>
                            <div class="uk-margin-left uk-text-muted uk-text-meta">260 отзывов</div>
                        </div>
                    </div>
                    <div>
                        <button class="uk-button uk-button-primary">+ Добавить</button>
                    </div>
                </div>
                <hr class="uk-margin">
                <div class="entity__row uk-childr" uk-grid>
                    <div class="entity__image uk-width-auto">
                        <img src="img/avatar.jpg" srcset="img/avatar@2x.jpg 2x" class="avatar" width="80" height="80">
                    </div>
                    <div class="uk-width-expand">
                        <div><a href="#">Universal home</a></div>
                        <div class="uk-flex">
                            <div>*****</div>
                            <div class="uk-margin-left uk-text-muted uk-text-meta">260 отзывов</div>
                        </div>
                    </div>
                    <div>
                        <button class="uk-button uk-button-primary">+ Добавить</button>
                    </div>
                </div>
                <hr class="uk-margin">
                <div class="uk-flex-center" uk-grid>
                    <button class="uk-button uk-button-default uk-text-muted uk-width-1-5">Отменить</button>
                    <button class="uk-button uk-button-primary uk-width-1-5">Закрыть</button>
                </div>
            </div>
</main>
<?php include 'footer.php'; ?>