<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>
<body>
<?php include 'header.php'; ?>
<main class="main-container uk-container uk-flex uk-flex-top">

    <!--    боковая панель-->
    <div class="sidebar c-bordered">
        <div class="c-heading-2">ПРОФИЛЬ</div>
        <div class="uk-button uk-button-small uk-background-muted">Недвижимость</div>
        <hr>
        <div class="uk-flex uk-flex-between">
            <div class="c-heading-3">Календарь</div>
            <span class="count">15 (5 сегодня)</span>
        </div>
        <hr>
        <div class="c-heading-3">Объекты</div>
        <div class="uk-flex uk-flex-between">
            <a href="/">Активные</a>
            <span class="count">2</span>
        </div>
        <div class="uk-flex uk-flex-between">
            <div class="c-heading-3">Входящие предложения</div>
            <div class="uk-badge">10</div>
        </div>
        <div class="uk-flex uk-flex-between">
            <div class="c-heading-3">Сообщения по объектам</div>
            <div class="uk-badge-reverse">2</div>
        </div>
        158 872 ₽
        <div class="uk-text-danger">заполнено 16%</div>
    </div>

    <div class="content">

        <!--        хлебные крошки, заголовок, кнопки справа от заголовка-->
        <div class="uk-container-custom-padding">
			<?php include 'breadcrumbs.php' ?>
            <div class="uk-flex uk-flex-nowrap uk-flex-between">
                <h1 class="uk-heading-primary">Просторная квартира-студия  в центре Калининграда</h1>
                <div class="uk-flex uk-flex-wrap c-buttons-wrapper-smaller">
                    <a href="/" class="c-icon c-icon-smaller uk-button uk-button-transparent">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g fill="none" fill-rule="evenodd" stroke="#4A90E2" stroke-linecap="round"
                               stroke-linejoin="round" stroke-width="2">
                                <path d="M19.068 18.094h3.072c.615 0 1.118-.504 1.118-1.118V7.524c0-.615-.503-1.118-1.118-1.118H2.013c-.616 0-1.119.503-1.119 1.118v9.452c0 .614.503 1.118 1.119 1.118h3.081"/>
                                <path d="M5.089 12.955v7.88c0 .615.503 1.118 1.118 1.118h11.738c.615 0 1.118-.503 1.118-1.119v-7.879M5.089 12.545h13.974M19.063 6.406V2.934c0-.572-.503-1.04-1.118-1.04H6.208c-.616 0-1.119.468-1.119 1.04v3.472"/>
                            </g>
                        </svg>

                        Версия для печати
                    </a>
                    <a href="/" class="c-icon c-icon-smaller uk-button uk-button-transparent">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g fill="none" fill-rule="evenodd" stroke="#4A90E2" stroke-linecap="round"
                               stroke-linejoin="round" stroke-width="2">
                                <path d="M19.499 2.96c0-.593-.486-1.079-1.08-1.079H5.461c-.594 0-1.08.486-1.08 1.08v17.793c0 .593.486 1.08 1.08 1.08h8.744c.595 0 1.081-.487 1.084-1.08l.005-2.048c.002-.595.49-1.08 1.083-1.08h2.042c.594 0 1.08-.487 1.08-1.08V2.96zM7.356 17.216h4.584M7.356 11.857h9.168M7.356 6.498h9.168"/>
                            </g>
                        </svg>
                        Сохранить PDF
                    </a>
                </div>
                <div class="uk-flex uk-flex-wrap c-buttons-wrapper-bigger">
                    <button class="uk-button uk-button-primary-inverse">Подать объявление</button>
                    <button class="uk-button uk-button-primary uk-box-shadow-large">Редактировать объект</button>
                </div>
            </div>
        </div>

        <!--        навигация по странице объекта-->
        <div class="uk-container-custom-padding">
            <div class="c-bordered uk-container-custom-padding uk-flex c-subnav">
                <a href="/" class="c-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <g fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"
                           stroke-width="2">
                            <path stroke="#4A90E2" d="M11.821 15.076v4.999"/>
                            <path stroke="#A8A8A8" d="M19.161 8.28v12.274H4.481V8.28l7.34-5.395z"/>
                        </g>
                    </svg>
                    Основные данные
                </a>
                <a href="/" class="c-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <g fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"
                           stroke-width="2">
                            <path stroke="#A8A8A8" d="M16.236 6.608V4.102H8.015v2.506H3.1v13.033h18.047V6.608z"/>
                            <path stroke="#4A90E2"
                                  d="M16.436 13.025a4.31 4.31 0 1 1-8.623 0 4.312 4.312 0 0 1 8.623 0z"/>
                        </g>
                    </svg>
                    Фото + Схемы + Документы
                </a>
                <a href="/" class="c-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <g fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"
                           stroke-width="2">
                            <path stroke="#A8A8A8"
                                  d="M12.268 18.04H8.474a3.874 3.874 0 0 1-1.452-7.466l-.477-1.66a2.676 2.676 0 0 1 3.402-3.282V4.068l.007-.055a2.28 2.28 0 0 1 4.533.055v1.564a3.01 3.01 0 0 1 3.597 3.992l-.368.95a4.108 4.108 0 0 1-2.367 7.466h-3.081z"/>
                            <path stroke="#4A90E2" d="M12.408 11.39v11.441"/>
                        </g>
                    </svg>
                    Обустройства и удобства
                </a>
                <a href="/" class="c-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <g fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"
                           stroke-width="2">
                            <path stroke="#A8A8A8" d="M4.5 2.375h15v19.25h-15z"/>
                            <path stroke="#4A90E2" d="M7.999 7.375h8.002M7.999 12.5h8.002"/>
                        </g>
                    </svg>
                    Описание
                </a>
                <a href="/" class="c-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <g fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"
                           stroke-width="2">
                            <path stroke="#A8A8A8" d="M4.5 2.375h15v19.25h-15z"/>
                            <path stroke="#4A90E2" d="M7.999 12.5h8.002M12 8.499v8.002"/>
                        </g>
                    </svg>
                    Дополнительно
                </a>
                <a href="/" class="c-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <g fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"
                           stroke-width="2">
                            <path stroke="#4A90E2" d="M20.203 5.04L12 12 3.797 5.04z"/>
                            <path stroke="#A8A8A8" d="M3.299 5.04H20.7v13.92H3.299z"/>
                        </g>
                    </svg>
                    Контакты
                </a>
            </div>
        </div>

        <div class="uk-container-custom-padding uk-flex uk-flex-top">
            <div class="b-slider">
                <div class="b-slider__container">
                    <div class="b-slider__slide">
                        <img src="http://placekitten.com/g/600/440" alt="" width="600" height="440">
                    </div>
                    <div class="b-slider__slide">
                        <img src="http://placekitten.com/g/600/440" alt="" width="600" height="440">
                    </div>
                    <div class="b-slider__slide">
                        <img src="http://placekitten.com/g/600/440" alt="" width="600" height="440">
                    </div>
                    <div class="b-slider__slide">
                        <img src="http://placekitten.com/g/600/440" alt="" width="600" height="440">
                    </div>
                    <div class="b-slider__slide">
                        <img src="http://placekitten.com/g/600/440" alt="" width="600" height="440">
                    </div>
                    <div class="b-slider__slide">
                        <img src="http://placekitten.com/g/600/440" alt="" width="600" height="440">
                    </div>
                    <div class="b-slider__slide">
                        <img src="http://placekitten.com/g/600/440" alt="" width="600" height="440">
                    </div>
                </div>
                <div class="b-slider__nav">
                    <div class="b-slider__nav-slide">
                        <img src="http://placekitten.com/g/104/80" alt="" width="104" height="80">
                    </div>
                    <div class="b-slider__nav-slide">
                        <img src="http://placekitten.com/g/104/80" alt="" width="104" height="80">
                    </div>
                    <div class="b-slider__nav-slide">
                        <img src="http://placekitten.com/g/104/80" alt="" width="104" height="80">
                    </div>
                    <div class="b-slider__nav-slide">
                        <img src="http://placekitten.com/g/104/80" alt="" width="104" height="80">
                    </div>
                    <div class="b-slider__nav-slide">
                        <img src="http://placekitten.com/g/104/80" alt="" width="104" height="80">
                    </div>
                    <div class="b-slider__nav-slide">
                        <img src="http://placekitten.com/g/104/80" alt="" width="104" height="80">
                    </div>
                    <div class="b-slider__nav-slide">
                        <img src="http://placekitten.com/g/104/80" alt="" width="104" height="80">
                    </div>
                </div>
            </div>

            <!--        инфо о пользователе справа-->
            <div>
                <div class="c-bordered uk-flex uk-container-custom-padding">
                    <div><img class="c-avatar" src="https://via.placeholder.com/64x64" alt="" width="64" height="64">
                    </div>
                    <div class="uk-width-1-1 uk-padding-left">
                        <div class="uk-flex uk-flex-nowrap uk-flex-between">
                            <div>Михаил<br>Аркадьевич</div>
                            <div class="uk-text-muted uk-text-small">На сайте с мая 2017</div>
                        </div>
                        <div class="uk-width-1-1">
                            <div class="uk-flex uk-flex-nowrap uk-flex-bottom">
                                <div>
                                    <a href="/">Selix Realty</a>
                                    <div class="c-rating">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24">
                                            <path fill="#FFB96D" fill-rule="evenodd"
                                                  d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                        </svg>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24">
                                            <path fill="#FFB96D" fill-rule="evenodd"
                                                  d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                        </svg>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24">
                                            <g fill="none" fill-rule="evenodd">
                                                <path fill="#D8D8D8"
                                                      d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                                <path fill="#FFB96D"
                                                      d="M12 1v18l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1z"/>
                                            </g>
                                        </svg>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24">
                                            <path fill="#D8D8D8" fill-rule="evenodd"
                                                  d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                        </svg>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24">
                                            <path fill="#D8D8D8" fill-rule="evenodd"
                                                  d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                        </svg>
                                    </div>
                                </div>
                                <span class="uk-text-small uk-text-muted uk-padding-left--smaller">Отзывы: 236</span>
                            </div>
                            <hr>
                            <div><span class="uk-text-muted">Комиссия:</span> 15%</div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-1 uk-flex uk-flex-right">
                    <button class="uk-button uk-button-primary-inverse">Показать телефон</button>
                    <button class="uk-button uk-button-primary uk-box-shadow-large">Написать сообщение</button>
                </div>
                <div class="uk-container-custom-padding">
                    <div class="uk-flex uk-flex-between">
                        <div>
                            <span class="uk-text-muted">ID: 980980988</span>
                            <div>Объект: 15267808</div>
                        </div>
                        <div>
                            <div class="c-icon uk-padding-small uk-padding-remove-top uk-padding-remove-left">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path fill="#4A90E2" fill-rule="evenodd"
                                          d="M21.444 2.5a.47.47 0 0 0-.172.026l-5.939 2.19L8.667 2.5 2.406 4.506c-.234.073-.406.263-.406.506v15.96c0 .29.25.528.556.528a.47.47 0 0 0 .172-.026l5.939-2.19 6.666 2.216 6.267-2c.233-.08.4-.27.4-.512V3.028c0-.29-.25-.528-.556-.528zm-6.11 16.889l-6.667-2.222V4.61l6.666 2.222V19.39z"/>
                                </svg>
                                <a href="/">На карте</a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                        <div class="uk-text-muted uk-width-1-3">Адрес</div>
                        <div class="uk-width-expand">898212, г. Калининград, Вишняковский р-н, ул. Яблочкова, д. 15,
                            стр. 8
                        </div>
                    </div>
                    <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                        <div class="uk-text-muted uk-width-1-3">Опубликовано</div>
                        <div class="uk-width-expand">25.08.2015 в 11:30</div>
                    </div>
                    <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                        <div class="uk-text-muted uk-width-1-3">Просмотров</div>
                        <div class="uk-width-expand">135</div>
                    </div>
                </div>
            </div>
        </div>

        <!--        инфо об объекте-->
        <div class="uk-container-custom-padding">
            <div class="uk-container c-bordered-blue">
                <div class="uk-container-custom-padding uk-flex-between" uk-grid>
                    <div class="uk-grid-divider uk-flex-nowrap" uk-grid>
                        <div class="c-two-sided-list c-width-25">
                            <div><span class="c-heading-2">7 500 000 ₽</span></div>
                            <div>1 235 ₽ за м²</div>
                            <div>Комиссия 3%</div>
                        </div>
                        <div class="c-two-sided-list c-width-25">
                            <div><span class="c-heading-2">120 м²</span></div>
                            <div>Кухня 17м²</div>
                            <div>Жилая 98м²</div>
                        </div>
                        <div class="c-two-sided-list c-width-25">
                            <div><span class="c-heading-2">3 комнаты</span></div>
                            <div>Новостройка</div>
                            <div>свободно с<br>12.05.17</div>
                        </div>
                        <div class="c-two-sided-list c-width-25">
                            <div><span class="c-heading-2">8 этаж из 23</span></div>
                            <div>Гараж/парковка</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-container-custom-padding">
            <div class="c-heading-2">ОБУСТРОЙСТВО И УДОБСТВА</div>
        </div>

        <div class="uk-flex uk-grid-match">
            <div class="uk-container-custom-padding">
                <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                    <div class="uk-text-muted uk-width-1-3">Тип дома</div>
                    <div class="uk-width-expand">Монолитный</div>
                </div>
                <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                    <div class="uk-text-muted uk-width-1-3">Год постройки</div>
                    <div class="uk-width-expand">2004</div>
                </div>
                <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                    <div class="uk-text-muted uk-width-1-3">Первичный рынок</div>
                    <div class="uk-width-expand">Да, Дата сдачи ГК, срок 1 кв. 2017 года</div>
                </div>
                <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                    <div class="uk-text-muted uk-width-1-3">Состояние объекта</div>
                    <div class="uk-width-expand">Новостройка</div>
                </div>
                <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                    <div class="uk-text-muted uk-width-1-3">Гараж/ парковка</div>
                    <div class="uk-width-expand">2 шт., 5000 ₽/мес, Наружное парковочное место</div>
                </div>
            </div>
            <div class="uk-container-custom-padding">
                <div class="uk-flex-between" uk-grid>
                    <div>Пассажирский лифт</div>
                    <div>Грузовой лифт</div>
                    <div>Объект культурного наследия</div>
                    <div>Подвал/ Кладовка</div>
                    <div>Вход без ступеней</div>
                </div>
            </div>
            <div class="uk-container-custom-padding">
                <div class="uk-flex-between" uk-grid>
                    <div>Терраса, Балкон, Сад, Встроенная кухня, Гостевой туалет, Отопление</div>
                </div>
            </div>
        </div>

        <div class="uk-flex uk-grid-match uk-flex-top">
            <div class="uk-container-custom-padding">
                <div class="c-heading-2">ПЛАН ПОМЕЩЕНИЯ</div>
                <br><br><br>
                <div class="b-popup">
                    <a class="b-popup__link" href="http://placekitten.com/g/1024/768">
                        <img class="b-popup__preview" src="http://placekitten.com/g/380/278" alt="" width="380"
                             height="278">
                    </a>
                </div>
            </div>
            <div class="uk-container-custom-padding">
                <div class="c-heading-2">ДОКУМЕНТЫ</div>
                <br><br><br>
                <a href="/" class="c-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <g fill="none" fill-rule="evenodd" stroke="#4A90E2" stroke-linecap="round"
                           stroke-linejoin="round" stroke-width="2">
                            <path d="M19.499 2.96c0-.593-.486-1.079-1.08-1.079H5.461c-.594 0-1.08.486-1.08 1.08v17.793c0 .593.486 1.08 1.08 1.08h8.744c.595 0 1.081-.487 1.084-1.08l.005-2.048c.002-.595.49-1.08 1.083-1.08h2.042c.594 0 1.08-.487 1.08-1.08V2.96zM7.356 17.216h4.584M7.356 11.857h9.168M7.356 6.498h9.168"/>
                        </g>
                    </svg>
                    Документ на квартиру 1
                </a>
                <br>
                <br>
                <a href="/" class="c-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <g fill="none" fill-rule="evenodd" stroke="#4A90E2" stroke-linecap="round"
                           stroke-linejoin="round" stroke-width="2">
                            <path d="M19.499 2.96c0-.593-.486-1.079-1.08-1.079H5.461c-.594 0-1.08.486-1.08 1.08v17.793c0 .593.486 1.08 1.08 1.08h8.744c.595 0 1.081-.487 1.084-1.08l.005-2.048c.002-.595.49-1.08 1.083-1.08h2.042c.594 0 1.08-.487 1.08-1.08V2.96zM7.356 17.216h4.584M7.356 11.857h9.168M7.356 6.498h9.168"/>
                        </g>
                    </svg>
                    Документ на квартиру 2
                </a>
            </div>
        </div>

        <div class="uk-container-custom-padding">
            <div class="c-heading-2">ОПИСАНИЕ</div>
        </div>

        <div class="uk-container-custom-padding">
            <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                <div class="uk-text-muted uk-width-1-4">Описание объекта</div>
                <div class="uk-width-expand">Старинный дом в античном стиле</div>
            </div>
            <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                <div class="uk-text-muted uk-width-1-4">Удобства</div>
                <div class="uk-width-expand">Дубовый паркет, Французские окна</div>
            </div>
        </div>

        <div class="uk-container-custom-padding">
            <div class="c-heading-2">ДОПОЛНИТЕЛЬНО</div>
        </div>

        <div class="uk-container-custom-padding">
            <div class="uk-flex uk-grid-match uk-flex-top">
                <div>
                    <div class="c-heading-3">ПЛАТЕЖИ</div>
                    <br><br>
                    <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                        <div class="uk-text-muted uk-width-1-4">Описание объекта</div>
                        <div class="uk-width-expand">Старинный дом в античном стиле</div>
                    </div>
                    <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                        <div class="uk-text-muted uk-width-1-4">Удобства</div>
                        <div class="uk-width-expand">Дубовый паркет, Французские окна</div>
                    </div>
                </div>
                <div>
                    <div class="c-heading-3">ПЛАТЕЖИ</div>
                    <br><br>
                    <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                        <div class="uk-text-muted uk-width-1-4">Описание объекта</div>
                        <div class="uk-width-expand">Старинный дом в античном стиле</div>
                    </div>
                    <div class="uk-flex-between uk-flex-nowrap" uk-grid>
                        <div class="uk-text-muted uk-width-1-4">Удобства</div>
                        <div class="uk-width-expand">Дубовый паркет, Французские окна</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-container-custom-padding">
            <div class="c-heading-2">ОБЪЕКТ НА КАРТЕ</div>
            <br><br>
            <div><span class="uk-text-muted">Адрес:</span>&nbsp;ул. Маршала Антонова</div>
            <br>
            <img src="http://placekitten.com/g/1024/320" alt="" width="1024" height="320">
        </div>


        <!--        инфо о пользователе снизу-->
        <div class="uk-container-custom-padding">
            <div class="c-bordered uk-flex uk-container-custom-padding">
                <div><img class="c-avatar" src="https://via.placeholder.com/64x64" alt="" width="64" height="64">
                </div>
                <div class="uk-width-1-1 uk-padding-left">
                    <div class="uk-flex uk-flex-nowrap uk-flex-between">
                        <div>Михаил<br>Аркадьевич</div>
                        <div class="uk-flex uk-flex-nowrap uk-flex-bottom">
                            <div>
                                <a href="/">Selix Realty</a>
                                <div class="c-rating">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path fill="#FFB96D" fill-rule="evenodd"
                                              d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path fill="#FFB96D" fill-rule="evenodd"
                                              d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <g fill="none" fill-rule="evenodd">
                                            <path fill="#D8D8D8"
                                                  d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                            <path fill="#FFB96D"
                                                  d="M12 1v18l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1z"/>
                                        </g>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path fill="#D8D8D8" fill-rule="evenodd"
                                              d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <path fill="#D8D8D8" fill-rule="evenodd"
                                              d="M12 19l-7.053 3.708 1.347-7.854L.587 9.292l7.886-1.146L12 1l3.527 7.146 7.886 1.146-5.707 5.562 1.347 7.854z"/>
                                    </svg>
                                </div>
                            </div>
                            <span class="uk-text-small uk-text-muted uk-padding-left--smaller">Отзывы: 236</span>
                        </div>
                        <div><span class="uk-text-muted">Комиссия:</span> 15%</div>
                        <div class="uk-text-muted uk-text-small">На сайте с мая 2017</div>
                    </div>
                    <div class="uk-width-1-1 uk-flex uk-flex-right">
                        <button class="uk-button uk-button-primary-inverse">Подать объявление</button>
                        <button class="uk-button uk-button-primary uk-box-shadow-large">Редактировать объект</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

</main>
<?php include 'footer.php' ?>
</body>
</html>