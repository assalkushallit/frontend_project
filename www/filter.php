<div class="uk-container-custom-padding uk-flex-between b-filter" uk-grid>
    <div class="b-filter__desktop">
        <div class="uk-flex uk-flex-nowrap">
            <div class="uk-grid-divider uk-width-1-1" uk-grid>
                <div class="uk-width-1-4">
                    <div class="uk-text-muted">Я хочу</div>
                    <div>
                        <select name="" id="" class="b-select">
                            <option value="">Купить</option>
                            <option value="">Продать</option>
                        </select>
                    </div>
                </div>
                <div class="uk-width-1-2 c-filter-location">
                    <div class="fa fa-location-arrow"></div>
                    <div class="uk-text-muted">Где?</div>
                    <div>
                        <input type="text" class="uk-input uk-form-large"
                               placeholder="Регион, индекс, район, улица">
                    </div>
                </div>
                <div class="uk-width-1-4">
                    <div class="uk-text-muted">Что?</div>
                    <div>
                        <select name="" id="" class="b-select">
                            <option value="">Жилая недвижимость</option>
                        </select>
                    </div>
                </div>
            </div>
            <div>
                <button class="uk-button uk-button-large uk-button-primary uk-box-shadow-large">Поиск &mdash;
                    69
                </button>
                <br>
            </div>
        </div>
        <div class="uk-flex uk-flex-nowrap">
            <div class="uk-width-1-1" uk-grid>
                <div class="c-input-with-rouble uk-width-1-4">
                    <input type="text" class="uk-input uk-form-underline"
                           placeholder="До">
                    <div class="c-rouble">₽</div>
                </div>
                <div class="uk-width-1-4">
                    <input type="text" class="uk-input uk-form-underline"
                           placeholder="Площадь до">
                </div>
                <div class="uk-width-1-4">
                    <select name="" id="" class="b-select b-select--smaller">
                        <option value="">Количество комнат</option>
                    </select>
                </div>
                <div class="uk-width-1-4">
                    <select name="" id="" class="b-select b-select--smaller">
                        <option value="">Удаление</option>
                        <option value="">Добавление</option>
                    </select>
                </div>
            </div>
            <div class="c-save-search"><a href="/">Сохранить поиск</a></div>
        </div>
        <div class="b-filter__mobile">
            <input type="text" placeholder="Что вы ищите?">
            <button type="submit"></button>
        </div>
    </div>
</div>