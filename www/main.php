<!DOCTYPE html>
<html>
<?php include 'head.php'; ?>
<body class="b-main">
<?php include 'header.php'; ?>

<main class="main-container uk-container uk-flex">

    <div class="b-main__content content">

        <div class="uk-container">
            <div class="b-main__header-img"></div>

            <h1 class="b-main__header">Предложения рынка недвижимости России</h1>

            <div class="b-main__filter c-bordered-white">
				<?php include 'filter.php'; ?>
            </div>
        </div>

        <div class="uk-container">
            <div class="b-main__section uk-container-custom-padding">
                <div class="b-main__section-header uk-flex uk-flex-middle">
                    <div class="c-heading-1">НОВЫЕ ОБЪЕКТЫ</div>
                    <div><span>85</span></div>
                    <div class="uk-flex-1"></div>
                    <div><a href="/">Смотреть все</a></div>
                </div>

                <div class="b-main-slider">
                    <div class="b-main-slider__slide uk-width-1-4 uk-flex uk-flex-column uk-flex-middle">
                        <img src="img/pic.png" srcset="img/pic@2x.png 2x, img/pic@3x.png 3x"
                             class="pic uk-first-column"
                        width="300"
                        height="240">
                        <div class="b-main-slider__slide-content">
                            <a href="/" class="c-heading-3">Необычная 3-х комнатная квартира на юге
                                Калининграда</a>
                            <br>
                            <br>
                            <div class="uk-text-muted">3-комн. кв., 120 м²</div>
                            <div class="uk-text-muted">ул. Маршала Антонова</div>
                            <br>
                            <div>45000 ₽/мес</div>
                        </div>
                    </div>

                    <div class="b-main-slider__slide uk-width-1-4 uk-flex uk-flex-column uk-flex-middle">
                        <img src="img/pic.png" srcset="img/pic@2x.png 2x, img/pic@3x.png 3x"
                             class="pic uk-first-column"
                             width="300"
                             height="240">
                        <div class="b-main-slider__slide-content">
                            <a href="/" class="c-heading-3">Необычная 3-х комнатная квартира на юге
                                Калининграда</a>
                            <br>
                            <br>
                            <div class="uk-text-muted">3-комн. кв., 120 м²</div>
                            <div class="uk-text-muted">ул. Маршала Антонова</div>
                            <br>
                            <div>45000 ₽/мес</div>
                        </div>
                    </div>

                    <div class="b-main-slider__slide uk-width-1-4 uk-flex uk-flex-column uk-flex-middle">
                        <img src="img/pic.png" srcset="img/pic@2x.png 2x, img/pic@3x.png 3x"
                             class="pic uk-first-column"
                             width="300"
                             height="240">
                        <div class="b-main-slider__slide-content">
                            <a href="/" class="c-heading-3">Необычная 3-х комнатная квартира на юге
                                Калининграда</a>
                            <br>
                            <br>
                            <div class="uk-text-muted">3-комн. кв., 120 м²</div>
                            <div class="uk-text-muted">ул. Маршала Антонова</div>
                            <br>
                            <div>45000 ₽/мес</div>
                        </div>
                    </div>

                    <div class="b-main-slider__slide uk-width-1-4 uk-flex uk-flex-column uk-flex-middle">
                        <img src="img/pic.png" srcset="img/pic@2x.png 2x, img/pic@3x.png 3x"
                             class="pic uk-first-column"
                             width="300"
                             height="240">
                        <div class="b-main-slider__slide-content">
                            <a href="/" class="c-heading-3">Необычная 3-х комнатная квартира на юге
                                Калининграда</a>
                            <br>
                            <br>
                            <div class="uk-text-muted">3-комн. кв., 120 м²</div>
                            <div class="uk-text-muted">ул. Маршала Антонова</div>
                            <br>
                            <div>45000 ₽/мес</div>
                        </div>
                    </div>

                    <div class="b-main-slider__slide uk-width-1-4 uk-flex uk-flex-column uk-flex-middle">
                        <img src="img/pic.png" srcset="img/pic@2x.png 2x, img/pic@3x.png 3x"
                             class="pic uk-first-column"
                             width="300"
                             height="240">
                        <div class="b-main-slider__slide-content">
                            <a href="/" class="c-heading-3">Необычная 3-х комнатная квартира на юге
                                Калининграда</a>
                            <br>
                            <br>
                            <div class="uk-text-muted">3-комн. кв., 120 м²</div>
                            <div class="uk-text-muted">ул. Маршала Антонова</div>
                            <br>
                            <div>45000 ₽/мес</div>
                        </div>
                    </div>

                    <div class="b-main-slider__slide uk-width-1-4 uk-flex uk-flex-column uk-flex-middle">
                        <img src="img/pic.png" srcset="img/pic@2x.png 2x, img/pic@3x.png 3x"
                             class="pic uk-first-column"
                             width="300"
                             height="240">
                        <div class="b-main-slider__slide-content">
                            <a href="/" class="c-heading-3">Необычная 3-х комнатная квартира на юге
                                Калининграда</a>
                            <br>
                            <br>
                            <div class="uk-text-muted">3-комн. кв., 120 м²</div>
                            <div class="uk-text-muted">ул. Маршала Антонова</div>
                            <br>
                            <div>45000 ₽/мес</div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="b-main__section uk-container-custom-padding">
                <div class="b-main__section-header uk-flex uk-flex-middle">
                    <div class="c-heading-1">АКТУАЛЬНЫЕ ОБЪЕКТЫ</div>
                    <div></div>
                    <div class="uk-flex-1"></div>
                    <div><a href="/">Смотреть все</a></div>
                </div>
            </div>
            <div class="b-main__section uk-container-custom-padding">
                <div class="b-main__section-header uk-flex uk-flex-middle">
                    <div class="c-heading-1">НОВОСТРОЙКИ</div>
                    <div><span>85</span></div>
                    <div class="uk-flex-1"></div>
                    <div><a href="/">Смотреть все</a></div>
                </div>
            </div>
            <div class="b-main__section uk-container-custom-padding b-main__section--gray">
                <div class="b-main__section-header uk-flex uk-flex-middle">
                    <div class="c-heading-1">ПРОФИЛИ ПОЛЬЗОВАТЕЛЕЙ</div>
                    <div></div>
                    <div class="uk-flex-1"></div>
                    <div><a href="/">Смотреть все</a></div>
                </div>

                <div class="b-main-slider b-main-slider--profiles">
                    <div class="b-main-slider__slide uk-width-1-5 uk-flex uk-flex-column uk-flex-middle uk-text-center">
                        <br>
                        <img src="img/avatar.jpg"
                             srcset="img/avatar@2x.jpg 2x"
                             class="avatar"
                             width="200"
                             height="184">
                        <div class="b-main-slider__slide-content">
                            <div class="uk-text-center uk-width-1-1">
                                <a href="/" class="c-heading-3 uk-text-global">Пётр Сергеев</a>
                            </div>
                            <br>
                            <div class="uk-text-muted">Профессиональный риэлтор с 10-ти летним опытом работы</div>
                            <br>
                            <a href="/" class="uk-button uk-button-muted uk-button-small">Посмотреть профиль</a>
                        </div>
                    </div>

                    <div class="b-main-slider__slide uk-width-1-5 uk-flex uk-flex-column uk-flex-middle uk-text-center">
                        <br>
                        <img src="img/avatar.jpg"
                             srcset="img/avatar@2x.jpg 2x"
                             class="avatar"
                             width="200"
                             height="184">
                        <div class="b-main-slider__slide-content">
                            <div class="uk-text-center uk-width-1-1">
                                <a href="/" class="c-heading-3 uk-text-global">Пётр Сергеев</a>
                            </div>
                            <br>
                            <div class="uk-text-muted">Профессиональный риэлтор с 10-ти летним опытом работы</div>
                            <br>
                            <a href="/" class="uk-button uk-button-muted uk-button-small">Посмотреть профиль</a>
                        </div>
                    </div>

                    <div class="b-main-slider__slide uk-width-1-5 uk-flex uk-flex-column uk-flex-middle uk-text-center">
                        <br>
                        <img src="img/avatar.jpg"
                             srcset="img/avatar@2x.jpg 2x"
                             class="avatar"
                             width="200"
                             height="184">
                        <div class="b-main-slider__slide-content">
                            <div class="uk-text-center uk-width-1-1">
                                <a href="/" class="c-heading-3 uk-text-global">Пётр Сергеев</a>
                            </div>
                            <br>
                            <div class="uk-text-muted">Профессиональный риэлтор с 10-ти летним опытом работы</div>
                            <br>
                            <a href="/" class="uk-button uk-button-muted uk-button-small">Посмотреть профиль</a>
                        </div>
                    </div>

                    <div class="b-main-slider__slide uk-width-1-5 uk-flex uk-flex-column uk-flex-middle uk-text-center">
                        <br>
                        <img src="img/avatar.jpg"
                             srcset="img/avatar@2x.jpg 2x"
                             class="avatar"
                             width="200"
                             height="184">
                        <div class="b-main-slider__slide-content">
                            <div class="uk-text-center uk-width-1-1">
                                <a href="/" class="c-heading-3 uk-text-global">Пётр Сергеев</a>
                            </div>
                            <br>
                            <div class="uk-text-muted">Профессиональный риэлтор с 10-ти летним опытом работы</div>
                            <br>
                            <a href="/" class="uk-button uk-button-muted uk-button-small">Посмотреть профиль</a>
                        </div>
                    </div>

                    <div class="b-main-slider__slide uk-width-1-5 uk-flex uk-flex-column uk-flex-middle uk-text-center">
                        <br>
                        <img src="img/avatar.jpg"
                             srcset="img/avatar@2x.jpg 2x"
                             class="avatar"
                             width="200"
                             height="184">
                        <div class="b-main-slider__slide-content">
                            <div class="uk-text-center uk-width-1-1">
                                <a href="/" class="c-heading-3 uk-text-global">Пётр Сергеев</a>
                            </div>
                            <br>
                            <div class="uk-text-muted">Профессиональный риэлтор с 10-ти летним опытом работы</div>
                            <br>
                            <a href="/" class="uk-button uk-button-muted uk-button-small">Посмотреть профиль</a>
                        </div>
                    </div>

                    <div class="b-main-slider__slide uk-width-1-5 uk-flex uk-flex-column uk-flex-middle uk-text-center">
                        <br>
                        <img src="img/avatar.jpg"
                             srcset="img/avatar@2x.jpg 2x"
                             class="avatar"
                             width="200"
                             height="184">
                        <div class="b-main-slider__slide-content">
                            <div class="uk-text-center uk-width-1-1">
                                <a href="/" class="c-heading-3 uk-text-global">Пётр Сергеев</a>
                            </div>
                            <br>
                            <div class="uk-text-muted">Профессиональный риэлтор с 10-ти летним опытом работы</div>
                            <br>
                            <a href="/" class="uk-button uk-button-muted uk-button-small">Посмотреть профиль</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>

    </div>

</main>
<?php include 'footer.php'; ?>
</body>
</html>