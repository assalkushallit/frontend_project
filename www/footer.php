<div class="uk-container uk-container-custom-padding b-footer">
	<div class="uk-flex-between" uk-grid>
		<div class="b-footer__copyrights">
			<a href="/"><img src="img/logo.svg" class="logo"></a><br><br>
			<div class="uk-text-muted">
				© 2017 Primarkt.ru<br>
				Все права защищены
			</div>
		</div>
		<div class="b-footer__nav">
			<ul class="uk-nav uk-nav-default">
				<li class="uk-active"><a href="/">Компания</a></li>
				<li><a href="/">О нас</a></li>
				<li><a href="/">Контакты</a></li>
				<li><a href="/">Карьера</a></li>
			</ul>
		</div>
		<div class="b-footer__nav">
			<ul class="uk-nav uk-nav-default">
				<li class="uk-active"><a href="/">Пресс-центр</a></li>
				<li><a href="/">Новости</a></li>
				<li><a href="/">СМИ о нас</a></li>
			</ul>
		</div>
		<div class="b-footer__nav">
			<ul class="uk-nav uk-nav-default">
				<li class="uk-active"><a href="/">Информация</a></li>
				<li><a href="/">О персональных данных</a></li>
				<li><a href="/">Реклама на сайте</a></li>
				<li><a href="/">F.A.Q.</a></li>
			</ul>
		</div>
		<div class="b-footer__socials">
			<a href="/" class="uk-icon-button"><i class="fa fa-facebook-f"></i></a>
			<a href="/" class="uk-icon-button"><i class="fa fa-twitter"></i></a>
			<a href="/" class="uk-icon-button"><i class="fa fa-vk"></i></a>
			<a href="/" class="uk-icon-button"><i class="fa fa-instagram"></i></a>
			<a href="/" class="uk-icon-button"><i class="fa fa-odnoklassniki"></i></a>
		</div>
	</div>
</div>

<script src="builds/dev/js/index.js"></script>